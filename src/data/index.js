import faker from "faker";

const randomInt = (min, max) =>
	Math.floor(Math.random() * (max - min + 1)) + min;

export const ingredients = [
	{ id: 1, name: "Spaghetti" },
	{ id: 2, name: "Tomatos" },
	{ id: 3, name: "Garlic" },
	{ id: 4, name: "Olive Oil" },
	{ id: 5, name: "Basil" },
	{ id: 6, name: "Paprika" },
	{ id: 7, name: "Pepper" },
	{ id: 8, name: "Salt" },
	{ id: 9, name: "Water" },
	{ id: 10, name: "Oregano" },
];

export const categories = [
	{ id: "cat1", name: "Category 1" },
	{ id: "cat2", name: "Category 2" },
	{ id: "cat3", name: "Category 3" },
	{ id: "cat4", name: "Category 4" },
	{ id: "cat5", name: "Category 5" },
];

export const quantityUnits = ["tbsp", "tsp", "kg", "l", "cups"];

let steps = [];
for (let i = 0; i < randomInt(6, 20); i++) {
	steps.push(faker.lorem.sentences());
}

export let recipes = [];
for (let i = 0; i < 10; i++) {
	let tmpDescr = faker.lorem.sentences();

	recipes.push({
		id: i + 1,
		title: faker.lorem.words(),
		author: {
			name: faker.name.firstName() + " " + faker.name.lastName(),
			url: "https://www.littlehandsblw.com",
		},
		short_description:
			tmpDescr.length > 100 ? tmpDescr.substring(0, 140) : tmpDescr,
		description:
			"<p>" +
			faker.lorem.paragraphs() +
			"</p><p><img src='" +
			process.env.PUBLIC_URL +
			"/images/recipes/recipe-big-2.jpg' style='width: 100%;' /></p><p>" +
			faker.lorem.paragraphs() +
			"</p><p><img src='" +
			process.env.PUBLIC_URL +
			"/images/recipes/recipe-big-3.jpg' style='width: 100%;' /></p><p>" +
			faker.lorem.paragraphs() +
			"</p>",
		url: "/recipe/" + faker.lorem.word(),
		imageUrl: process.env.PUBLIC_URL + "/images/recipes/" + i + ".jpg",
		ingredients: ingredients
			.filter((ing) => {
				return randomInt(0, 1) === 1;
			})
			.map((ing) => {
				return {
					quantity: randomInt(1, 50),
					quantity_unit: quantityUnits[randomInt(0, 4)],
					...ing,
				};
			}),
		categories: [
			categories.filter((cat) => {
				return randomInt(0, 1) === 1;
			}),
		],
		overview: {
			preparation: randomInt(5, 15),
			cook: randomInt(10, 90),
			wait: randomInt(0, 30),
			difficulty: getDifficultyVerb(randomInt(2, 5)),
		},
		steps: steps,
	});
}

function getDifficultyVerb(diff) {
	switch (diff) {
		case 2:
			return "Easy";
		case 3:
			return "Medium";
		case 4:
			return "Experienced";
		case 5:
			return "Hard";
		default:
			return "";
	}
}

export let comments = [];
for (let i = 0; i < 100; i++) {
	comments.push({
		id: "comm" + i,
		title: faker.lorem.words(),
		comment: faker.lorem.paragraph(),
		author: {
			id: "usr" + i,
			name: faker.name.firstName() + " " + faker.name.lastName(),
			avatar: faker.image.avatar(),
		},
		date: faker.date.past(),
		recipeID: randomInt(1, 10),
	});
}

export const userData = {
	name: faker.name.firstName() + " " + faker.name.lastName(),
	avatar: faker.image.avatar(),
	cover: process.env.PUBLIC_URL + "/images/profile-cover.jpg",
	joined: faker.date.past(),
	bio: faker.lorem.paragraphs(),
	favourites: [1, 3, 5, 7, 9, 10],
};
