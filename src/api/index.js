import { recipes, comments, ingredients, categories, userData } from "../data";

const api = {
	searchRecipesCount: (ingrList = []) => {
		// Search recipes and find those that contain the selected ingredients
		if (ingrList.length > 0) {
			const recipesFound = recipes.filter((recipe) => {
				const recipeIngsFound = recipe.ingredients.filter((ingredient) => {
					const searchIngsFound = ingrList.filter((searchIngr) => {
						return searchIngr.id === ingredient.id ? searchIngr : false;
					});
					return searchIngsFound.length > 0 ? ingredient : false;
				});

				return recipeIngsFound.length === ingrList.length ? recipe : false;
			});

			return recipesFound.length;
		} else {
			return 0;
		}
	},
	searchRecipes: (filters = {}) => {
		// Search recipes and find those that contain the selected ingredients and return them
		const ingrList = filters.ingredients;
		if (ingrList.length > 0) {
			const recipesFound = recipes.filter((recipe) => {
				const recipeIngsFound = recipe.ingredients.filter((ingredient) => {
					const searchIngsFound = ingrList.filter((searchIngr) => {
						return searchIngr === ingredient.id ? searchIngr : false;
					});
					return searchIngsFound.length > 0 ? ingredient : false;
				});

				return recipeIngsFound.length === ingrList.length ? recipe : false;
			});

			return recipesFound;
		} else {
			return [];
		}
	},
	getComments: (recID) => {
		if (recID) {
			return comments.filter((comment) => {
				return comment.recipeID === recID ? comment : false;
			});
		}

		return {};
	},
	getIngredients: () => {
		return ingredients;
	},
	getCategories: () => {
		return categories;
	},
	getRecipeDetails: (recID) => {
		if (recID) {
			return recipes.filter((recipe) => {
				if (recipe.id === recID) {
					return recipe;
				}

				return false;
			})[0];
		}

		return undefined;
	},
	getUserData: () => {
		return userData;
	},
	getRecipes: (recIDs = []) => {
		if (recIDs.length > 0) {
			return recIDs.map((recID) => {
				return recipes.filter((recipe) => {
					return recipe.id === recID;
				})[0];
			});
		} else {
			return [];
		}
	},
};

export default api;
