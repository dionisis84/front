import React from "react";
import Skeleton from "@material-ui/lab/Skeleton";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";

export default function RecipesListSkeleton(props){
	return(
		<Card>
			<CardActionArea>
				<Skeleton variant="rect" width="100%" height={200} />

				<CardContent>
					<Skeleton />

					<Skeleton width="60%" />
				</CardContent>
			</CardActionArea>
		</Card>
	);
}