import React from "react";
import api from "../../api";
import Grid from "@material-ui/core/Grid";
import RecipesListItem from "../RecipesListItem";
import RecipesListSkeleton from "../RecipesListSkeleton";
import Fade from "@material-ui/core/Fade";

export default function RecipesList(props) {
	const recipes = props.recipes
		? api.getRecipes(props.recipes)
		: api.searchRecipes({ ingredients: [1] });
	const { loading } = props;

	return (
		<React.Fragment>
			<Grid container>
				{!loading &&
					recipes.map((recipe) => {
						return (
							<Grid
								item
								xl={3}
								lg={4}
								md={4}
								sm={6}
								xs={12}
								style={{ padding: "20px" }}
								key={recipe.id + "-rec"}
							>
								{!loading && (
									<Fade in={!loading}>
										<RecipesListItem recipe={recipe} />
									</Fade>
								)}
							</Grid>
						);
					})}

				{/* Show the skeleton while results are loading */}
				{loading &&
					[1, 2, 3, 4, 5, 6].map((recipe, idx) => {
						return (
							<Grid
								item
								xl={3}
								lg={4}
								md={4}
								sm={6}
								xs={12}
								style={{ padding: "20px" }}
								key={idx + "-recskel"}
							>
								{loading && <RecipesListSkeleton in={loading} />}
							</Grid>
						);
					})}
			</Grid>
		</React.Fragment>
	);
}
