import React from "react";
import Popover from "@material-ui/core/Popover";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import LinearProgress from "@material-ui/core/LinearProgress";

const LoginPopover = (props) => {
	const { setDidLogin, color, contained } = props;
	const [loginLoading, setLoginLoading] = React.useState(false);
	const [username, setUsername] = React.useState("");
	const [password, setPassword] = React.useState("");
	const [anchorEl, setAnchorEl] = React.useState(null);
	const loginOpen = Boolean(anchorEl);

	const handleLoginClick = () => {
		setLoginLoading(true);

		setTimeout(() => {
			if (username === "wrong" && password === "wrong") {
				alert("Wrong credentials...");
			} else {
				setDidLogin(true);
				handleClose();
			}
		}, 1000);
	};

	const handleKeyPress = (e) => {
		if (e.key === "Enter" && username !== "" && password !== "") {
			handleLoginClick();
		}
	};

	const handleShowClick = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const handleClose = () => {
		setAnchorEl(null);
	};

	return (
		<React.Fragment>
			<Button
				color={color}
				onClick={handleShowClick}
				variant={contained ? "contained" : "text"}
				fullWidth={contained}
			>
				Login
			</Button>

			<Popover
				anchorOrigin={{
					vertical: "bottom",
					horizontal: "center",
				}}
				transformOrigin={{
					vertical: "top",
					horizontal: "center",
				}}
				open={loginOpen}
				onClose={handleClose}
				anchorEl={anchorEl}
				{...props}
			>
				<Box style={{ padding: "20px", maxWidth: "250px" }}>
					<Typography variant="h6">Login</Typography>

					<Grid
						container
						alignItems="center"
						direction="column"
						justify="center"
					>
						<Grid xs={12}>
							<TextField
								label="Username"
								fullWidth
								margin="dense"
								disabled={loginLoading}
								value={username}
								onChange={(e) => {
									setUsername(e.target.value);
								}}
								onKeyPress={handleKeyPress}
								autoFocus={true}
							/>
						</Grid>
						<Grid xs={12}>
							<TextField
								label="Password"
								fullWidth
								margin="dense"
								type="password"
								disabled={loginLoading}
								value={password}
								onChange={(e) => {
									setPassword(e.target.value);
								}}
								onKeyPress={handleKeyPress}
							/>
						</Grid>
						<Grid xs={12} style={{ paddingTop: "20px" }}>
							<Button
								startIcon={<ExitToAppIcon />}
								variant="contained"
								color="primary"
								onClick={handleLoginClick}
								disabled={username === "" || password === ""}
							>
								Login
								<div
									style={{ width: "100%", position: "absolute", bottom: "0px" }}
								>
									{loginLoading && <LinearProgress />}
								</div>
							</Button>
						</Grid>
					</Grid>
				</Box>
			</Popover>
		</React.Fragment>
	);
};

export default LoginPopover;
