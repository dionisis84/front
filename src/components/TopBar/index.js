import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Logo from "../Logo";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Hidden from "@material-ui/core/Hidden";
import LoginPopover from "../LoginPopover";
import LoggedInAvatar from "../LoggedInAvatar";
import faker from "faker";

const useStyles = makeStyles((theme) => ({
	appBar: {
		zIndex: theme.zIndex.drawer + 1,
	},
}));

export default function TopBar(props) {
	const classes = useStyles();
	const [didLogin, setDidLogin] = React.useState(false);
	const [userInfo, setUserInfo] = React.useState({
		name: faker.name.firstName() + " " + faker.name.lastName(),
		avatar: faker.image.avatar(),
	});

	return (
		<AppBar position="fixed" className={classes.appBar}>
			<Toolbar>
				{props.showDrawer && (
					<Hidden mdUp>
						<IconButton
							color="inherit"
							aria-label="open drawer"
							onClick={props.handleDrawerToggle}
							edge="start"
						>
							<MenuIcon />
						</IconButton>
					</Hidden>
				)}

				<Link href="/" underline="none">
					<Logo width="150" variant="h4" edge="start" />
				</Link>

				<Typography variant="h6" style={{ flexGrow: 1, marginLeft: "20px" }}>
					{props.title ? props.title : ""}
				</Typography>

				{/* Mobile version, hide popover if small device and page is recipes list */}
				{!didLogin && props.showDrawer && (
					<Hidden smDown>
						<LoginPopover setDidLogin={setDidLogin} color="secondary" />
					</Hidden>
				)}

				{/* Show the popover on recipe page */}
				{!didLogin && !props.showDrawer && (
					<LoginPopover setDidLogin={setDidLogin} color="secondary" />
				)}

				{didLogin && (
					<LoggedInAvatar user={userInfo} setDidLogin={setDidLogin} />
				)}
			</Toolbar>
		</AppBar>
	);
}
