import React from "react";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import StyledTextField from "../StyledTextField";
import emailjs from 'emailjs-com';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import LinearProgress from "@material-ui/core/LinearProgress";
import { makeStyles } from '@material-ui/core/styles';

function Alert(props) {
	return <MuiAlert elevation={6} variant="filled" {...props} />;
};

const useStyles = makeStyles((theme) => ({
	root: {
		width: '100%',
		'& > * + *': {
		  marginTop: theme.spacing(2),
		},
		position: "absolute",
		bottom: "0px"
	  }
}));

function MyLinearProgress(props){
	const classes = useStyles();
	const {formLoading} = props;
	
	return(
		<div className={classes.root}>{formLoading && <LinearProgress />}</div>
	);
}

class FooterContact extends React.Component{
	state = {
		openAlert : {
			open: false, 
			reason: "",
			type: ""
		},
		formData: {
			name: "",
			email: "",
			message: ""
		},
		formSubmitDisable: false,
		formLoading: false
	}
	
	sendEmail = (e) => {
		e.preventDefault();

		this.setState({formSubmitDisable: true, formLoading: true});
	
		emailjs.sendForm('sendgrid', 'recipes_project', e.target, 'user_UNWx9jgjvqmRfVBjVYd4d')
		  .then((result) => {
			  this.setState({
				  openAlert: {
					  open: true, 
					  reason: "Your message was sent succesfully!", 
					  type: "success"
				}, 
				formData: {
					name: "", 
					email: "", 
					message: ""}, 
					formSubmitDisable: false, 
					formLoading: false
				})
		  }, (error) => {
			this.setState({
				openAlert: {
					open: true, 
					reason: "Your message was not sent...", 
					type: "error"}, 
					formSubmitDisable: false, 
					formLoading: false
				})
		  });
	}

	handleClose = (event, reason) => {
		if (reason === 'clickaway') {
		  return;
		}
	
		this.setState({openAlert: {open: false, reason: "", type: ""}})
	};
	
	render(){
		return(
			<React.Fragment>
				<Typography variant="h6" color="secondary">Contact Dionisis</Typography>
				<Typography variant="body2" color="secondary">(This form actually works!)</Typography>
	
				<form onSubmit={this.sendEmail}>
					<StyledTextField 
						required 
						id="txtName" 
						label="Your name" 
						fullWidth
						margin="dense"
						color="secondary"
						name="contact_name"
						value={this.state.formData.name}
						onChange={(e) => {this.setState({formData: {name: e.target.value}})}}
					/>
	
					<StyledTextField 
						required 
						id="txtEmail" 
						label="Your email address" 
						type="email" 
						fullWidth
						margin="dense"
						color="secondary"
						name="contact_email"
						value={this.state.formData.email}
						onChange={(e) => {this.setState({formData: {email: e.target.value}})}}
					/>
	
					<StyledTextField
						id="txtComment"
						label="Your comment"
						multiline
						rows={4}
						fullWidth
						margin="dense"
						required
						color="secondary"
						name="contact_message"
						value={this.state.formData.message}
						onChange={(e) => {this.setState({formData: {message: e.target.value}})}}
					/>
	
					<Button variant="contained" color="secondary" style={{marginTop: "20px"}} type="submit" disabled={this.state.formSubmitDisable}>
						Send your message
						<MyLinearProgress formLoading={this.state.formLoading} />
					</Button>
				</form>
	
				<Snackbar open={this.state.openAlert.open} autoHideDuration={2000} onClose={this.handleClose}>
					<Alert onClose={this.handleClose} severity={this.state.openAlert.type}>
						{this.state.openAlert.reason}
					</Alert>
				</Snackbar>
			</React.Fragment>
		);
	};
};

export default FooterContact;