import React from "react";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';

export default function RecipeComment(props) {
	const {comments} = props;
	
	return(
		<List>
			{comments.map((comment) => {
				return(
					<React.Fragment key={comment.id}>
						<ListItem alignItems="flex-start">
							<ListItemAvatar>
								<Avatar alt={comment.author.name} src={comment.author.avatar} />
							</ListItemAvatar>

							<ListItemText
								primary={comment.title}
								secondary={
									<React.Fragment>
										<Typography
											component="span"
											variant="body2"
										>
											{comment.author.name + " - " + comment.date}
										</Typography>
										<br />
										<Typography 
											variant="body2" 
											color="textPrimary"
											component="span"
										>
											{comment.comment}
										</Typography>
									</React.Fragment>
								}
							/>
						</ListItem>
						<Divider variant="inset" component="li" />
					</React.Fragment>
				);
			})}
		</List>
	);
};