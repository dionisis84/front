import React from "react";
import HomePage from "../pages/HomePage";
import RecipesPage from "../pages/RecipesPage";
import ProfilePage from "../pages/ProfilePage";
import RecipePage from "../pages/RecipePage";
import RegisterUserPage from "../pages/RegisterUserPage";
import CssBaseline from "@material-ui/core/CssBaseline";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { ThemeProvider } from "@material-ui/core/styles";
import { theme } from "../Theme";

class App extends React.Component {
	render() {
		return (
			<ThemeProvider theme={theme}>
				<CssBaseline />
				<Router>
					<Switch>
						<Route path="/profile">
							<ProfilePage />
						</Route>
						<Route path="/register">
							<RegisterUserPage />
						</Route>
						<Route path="/recipe">
							<RecipePage />
						</Route>
						<Route path="/recipes">
							<RecipesPage />
						</Route>
						<Route path="/">
							<HomePage />
						</Route>
					</Switch>
				</Router>
			</ThemeProvider>
		);
	}
}

export default App;
