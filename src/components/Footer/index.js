import React from "react";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Logo from "../Logo";
import Typography from "@material-ui/core/Typography";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InstagramIcon from '@material-ui/icons/Instagram';
import FacebookIcon from '@material-ui/icons/Facebook';
import PinterestIcon from '@material-ui/icons/Pinterest';
import Link from "@material-ui/core/Link";
import FooterContact from "../FooterContact";
import {makeStyles} from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
	root: {
	  backgroundColor: "white",
	  marginBottom: "20px"
	},
	footerBox: {
		zIndex: theme.zIndex.drawer + 1,
		position: "relative",
		width: "100%",
		backgroundColor: "#f44336",
		padding: "20px"
	}
}));

export default function Footer(props){
	const classes = useStyles();
	
	return(
		<Box 
			boxShadow={3}
			className={classes.footerBox}
		>
			<Grid container>
				<Grid item xs={12} sm={12} md={6} lg={4} style={{paddingBottom: "20px"}}>
					<Logo variant="h5" project={true} />

					<Typography variant="body2" color="secondary" component="div">
						<p>This is a demo website project created by <Link href="https://www.linkedin.com/in/dionisis-kladis-512a413b/" target="_blank" rel="noopener" color="secondary">Dionisis Kladis</Link> and is a work in progress. Technologies used are:</p>
						<ul>
							<li><Link href="https://reactjs.org/" target="_blank" rel="noopener" color="secondary">React</Link></li>
							<li><Link href="https://material-ui.com/" target="_blank" rel="noopener" color="secondary">Material-UI</Link></li>
							{/* <li><Link href="https://redux.js.org/" target="_blank" rel="noopener" color="secondary">Redux</Link></li> */}
							<li><Link href="https://reacttraining.com/react-router/web/guides/quick-start" target="_blank" rel="noopener" color="secondary">React Router</Link></li>
							<li><Link href="https://styled-components.com/" target="_blank" rel="noopener" color="secondary">styled components</Link></li>
						</ul>
						<p><Link href="https://bitbucket.org/dionisis84/front/src/master/" target="_blank" rel="noopener" color="secondary">Source code can be found here.</Link></p>
						<p>Note: There is no backend API supporting this project. All demo data are defined in JSON objects and the functions that would call API endpoints retrieve data from those JSON</p>
					</Typography>
				</Grid>
				<Grid item xs={12} sm={12} md={6} lg={4} style={{paddingBottom: "20px"}}>
					<Typography variant="h6" color="secondary">Our Social Media</Typography>
					
					<List>
						<Link href="#">
							<ListItem>
								<ListItemIcon>
									<InstagramIcon color="secondary" />
								</ListItemIcon>
								<ListItemText primary="Instagram" primaryTypographyProps={{color: "secondary"}} />
							</ListItem>
						</Link>
						<Link href="#">
							<ListItem>
								<ListItemIcon>
									<PinterestIcon color="secondary" />
								</ListItemIcon>
								<ListItemText primary="Pinterest" primaryTypographyProps={{color: "secondary"}} />
							</ListItem>
						</Link>
						<Link href="#">
							<ListItem>
								<ListItemIcon>
									<FacebookIcon color="secondary" />
								</ListItemIcon>
								<ListItemText primary="Facebook" primaryTypographyProps={{color: "secondary"}} />
							</ListItem>
						</Link>
					</List>
				</Grid>
				<Grid item xs={12} sm={12} md={12} lg={4}>
					<FooterContact />
				</Grid>
			</Grid>
		</Box>
	);
};