import React from "react";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import FavoriteIcon from "@material-ui/icons/Favorite";
import PrintIcon from "@material-ui/icons/Print";
import ShareIcon from "@material-ui/icons/Share";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Box from "@material-ui/core/Box";
import Link from "@material-ui/core/Link";
import Hidden from "@material-ui/core/Hidden";
import ShareMenu from "../ShareMenu";
import { theme } from "../Theme";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import Tooltip from "@material-ui/core/Tooltip";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import LinearProgress from "@material-ui/core/LinearProgress";
import { makeStyles } from "@material-ui/core/styles";

function Alert(props) {
	return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
	root: {
		width: "100%",
		"& > * + *": {
			marginTop: theme.spacing(2),
		},
		position: "absolute",
		bottom: "0px",
	},
}));

const timeConvert = (n) => {
	let rhours = Math.floor(n / 60);
	let minutes = (n / 60 - rhours) * 60;
	let rminutes = Math.round(minutes);
	return (
		(rhours < 10 ? "0" + rhours : rhours) +
		":" +
		(rminutes < 10 ? "0" + rminutes : rminutes)
	);
};

const getDiffLevel = (levelInt) => {
	switch (levelInt) {
		case 2:
			return "Easy";
		case 3:
			return "Medium";
		case 4:
			return "Experienced";
		case 5:
			return "Hard";
		default:
			return "";
	}
};

export default function RecipesListItem(props) {
	const { recipe } = props;
	const [open, setOpen] = React.useState(false);
	const [openFavAlert, setOpenFavAlert] = React.useState(false);
	const anchorRef = React.useRef(null);
	const { preparation, wait, cook, difficulty } = recipe.overview;
	const [fav, setFav] = React.useState(!!+Math.round(Math.random()));
	const [favLoading, setFavLoading] = React.useState(false);
	const classes = useStyles();

	const handleToggle = () => {
		setOpen((prevOpen) => !prevOpen);
	};

	const handleClose = (event) => {
		if (anchorRef.current && anchorRef.current.contains(event.target)) {
			return;
		}

		setOpen(false);
	};

	const handleFavClick = () => {
		setFavLoading(true);
		setTimeout(() => {
			setFav(!fav);
			setOpenFavAlert(true);
			setFavLoading(false);
		}, 700);
	};

	const handleFavClose = (event, reason) => {
		if (reason === "clickaway") {
			return;
		}

		setOpenFavAlert(false);
	};

	return (
		<React.Fragment>
			<Box boxShadow={3} {...props}>
				<Card>
					<Link href={recipe.url} underline="none" color="inherit">
						<CardActionArea>
							<CardMedia
								image={recipe.imageUrl}
								title={recipe.title}
								style={{ height: "200px" }}
							/>

							<CardContent>
								<Box>
									<Grid container>
										<Tooltip
											title={
												<React.Fragment>
													<Grid container>
														<Grid
															item
															xs={4}
															style={{ padding: "10px", textAlign: "center" }}
														>
															<Typography
																variant="body2"
																style={{ fontWeight: "bold" }}
															>
																{"Prep"}
															</Typography>
															<Typography variant="body2">
																{timeConvert(preparation)}
															</Typography>
														</Grid>
														<Grid
															item
															xs={4}
															style={{ padding: "10px", textAlign: "center" }}
														>
															<Typography
																variant="body2"
																style={{ fontWeight: "bold" }}
															>
																{"Wait"}
															</Typography>
															<Typography variant="body2">
																{timeConvert(wait)}
															</Typography>
														</Grid>
														<Grid
															item
															xs={4}
															style={{ padding: "10px", textAlign: "center" }}
														>
															<Typography
																variant="body2"
																style={{ fontWeight: "bold" }}
															>
																{"Cook"}
															</Typography>
															<Typography variant="body2">
																{timeConvert(cook)}
															</Typography>
														</Grid>
													</Grid>
												</React.Fragment>
											}
											arrow
										>
											<Grid
												item
												xl={6}
												lg={6}
												md={6}
												sm={6}
												xs={6}
												style={{ textAlign: "center" }}
											>
												<Typography
													variant="body2"
													component="span"
													style={{ fontWeight: "300" }}
												>
													{"Duration: "}
												</Typography>
												<Typography
													variant="body2"
													component="span"
													color="primary"
													style={{ fontWeight: "bold" }}
												>
													{timeConvert(preparation + wait + cook)}
												</Typography>
											</Grid>
										</Tooltip>
										<Grid
											item
											xl={6}
											lg={6}
											md={6}
											sm={6}
											xs={6}
											style={{ textAlign: "center" }}
										>
											<Typography
												variant="body2"
												component="span"
												style={{ fontWeight: "300" }}
											>
												{"Difficulty: "}
											</Typography>
											<Typography
												variant="body2"
												component="span"
												color="primary"
												style={{ fontWeight: "bold" }}
											>
												{difficulty}
											</Typography>
										</Grid>
									</Grid>

									<Divider
										style={{ marginTop: "10px", marginBottom: "10px" }}
									/>
								</Box>

								<Typography gutterBottom variant="h5" component="h2">
									{recipe.title}
								</Typography>

								<Typography variant="body2" color="textSecondary" component="p">
									{recipe.short_description}
								</Typography>
							</CardContent>
						</CardActionArea>
					</Link>

					<CardActions>
						<Link href={recipe.url} underline="none">
							<Button variant="contained" size="small" color="primary">
								View <Hidden lgDown>Recipe</Hidden>
							</Button>
						</Link>

						<Button
							size="small"
							style={{ color: theme.palette.secondary.dark }}
							onClick={() => {
								handleFavClick();
							}}
						>
							<div className={classes.root}>
								{favLoading && <LinearProgress />}
							</div>
							{!fav && (
								<Tooltip title="Add to favourites" arrow>
									<FavoriteBorderIcon />
								</Tooltip>
							)}
							{fav && (
								<Tooltip title="Remove from favourites" arrow>
									<FavoriteIcon color="primary" />
								</Tooltip>
							)}
						</Button>

						<Button
							size="small"
							style={{ color: theme.palette.secondary.dark }}
						>
							<Tooltip title="Print recipe" arrow>
								<PrintIcon />
							</Tooltip>
						</Button>

						<Button
							size="small"
							style={{ color: theme.palette.secondary.dark }}
							onClick={handleToggle}
							ref={anchorRef}
						>
							<Tooltip title="Share recipe" arrow>
								<ShareIcon />
							</Tooltip>
						</Button>
						<ShareMenu
							open={open}
							anchorRef={anchorRef}
							handleClose={handleClose}
						/>
					</CardActions>
				</Card>
			</Box>

			<Snackbar
				open={openFavAlert}
				autoHideDuration={2000}
				onClose={handleFavClose}
			>
				<Alert onClose={handleFavClose} severity="success">
					{fav && "Recipe added succesfully to your favourites!"}
					{!fav && "Recipe removed succesfully from your favourites!"}
				</Alert>
			</Snackbar>
		</React.Fragment>
	);
}
