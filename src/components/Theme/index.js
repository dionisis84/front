import { createMuiTheme } from '@material-ui/core/styles';
import red from '@material-ui/core/colors/red';
// import white from '@material-ui/core/colors/white';

export const theme = createMuiTheme({
	palette: {
	  primary: red,
	  secondary: {
		  main: "#fafafa"
	  },
	},
	status: {
	  danger: 'orange',
	},
  });