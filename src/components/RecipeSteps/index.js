import React from "react";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Paper from '@material-ui/core/Paper';
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";

export default function RecipeSteps(props){
	const {steps} = props;
	
	return(
		<Paper style={{padding: "10px", marginBottom: "20px"}}>
			<Typography variant="h5">Recipe Steps</Typography>
			
			<List>
				{steps.map((step, idx) => {
					return(
						<React.Fragment key={idx}>
							<ListItem>
								<ListItemIcon>
									<Typography variant="h5" color="primary">{idx + 1}</Typography>
								</ListItemIcon>
				
								<ListItemText primary={<div>{step}</div>} />
							</ListItem>

							<Divider variant="inset" component="li" style={{marginTop: "10px", marginBottom: "10px"}} />
						</React.Fragment>
					);
				})}
			</List>
		</Paper>
	);
};