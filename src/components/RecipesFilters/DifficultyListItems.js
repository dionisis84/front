import React from "react";
import Typography from "@material-ui/core/Typography";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import Divider from "@material-ui/core/Divider";

export default function DifficultyListItems(props){
	const [checked, setChecked] = React.useState([-1]);

	const handleToggle = (value) => () => {
		const currentIndex = checked.indexOf(value);
		const newChecked = [...checked];
	
		if (currentIndex === -1) {
		  newChecked.push(value);
		} else {
		  newChecked.splice(currentIndex, 1);
		}
	
		setChecked(newChecked);
	};
	
	return(
		<React.Fragment>
			<Divider />
			<ListItem style={{marginTop: "20px"}}>
				<Typography variant="h6">Difficulty</Typography>
			</ListItem>
			
			<ListItem button dense onClick={handleToggle(0)}>
				<ListItemIcon>
					<Checkbox
						edge="start"
						checked={checked.indexOf(0) !== -1}
						disableRipple
						color="primary"
						inputProps={{ 'aria-labelledby': "lblDiffFilter0" }}
					/>
				</ListItemIcon>
				<ListItemText id={"lblDiffFilter0"} primary="Very Easy" />
			</ListItem>

			<ListItem button dense onClick={handleToggle(1)}>
				<ListItemIcon>
					<Checkbox
						edge="start"
						checked={checked.indexOf(1) !== -1}
						disableRipple
						color="primary"
						inputProps={{ 'aria-labelledby': "lblDiffFilter1" }}
					/>
				</ListItemIcon>
				<ListItemText id={"lblDiffFilter1"} primary="Easy" />
			</ListItem>

			<ListItem button dense onClick={handleToggle(2)}>
				<ListItemIcon>
					<Checkbox
						edge="start"
						checked={checked.indexOf(2) !== -1}
						disableRipple
						color="primary"
						inputProps={{ 'aria-labelledby': "lblDiffFilter2" }}
					/>
				</ListItemIcon>
				<ListItemText id={"lblDiffFilter2"} primary="Medium" />
			</ListItem>

			<ListItem button dense onClick={handleToggle(3)}>
				<ListItemIcon>
					<Checkbox
						edge="start"
						checked={checked.indexOf(3) !== -1}
						disableRipple
						color="primary"
						inputProps={{ 'aria-labelledby': "lblDiffFilter3" }}
					/>
				</ListItemIcon>
				<ListItemText id={"lblDiffFilter3"} primary="Experienced" />
			</ListItem>

			<ListItem button dense onClick={handleToggle(4)}>
				<ListItemIcon>
					<Checkbox
						edge="start"
						checked={checked.indexOf(4) !== -1}
						disableRipple
						color="primary"
						inputProps={{ 'aria-labelledby': "lblDiffFilter4" }}
					/>
				</ListItemIcon>
				<ListItemText id={"lblDiffFilter4"} primary="Hard" />
			</ListItem>
		</React.Fragment>
	);
};