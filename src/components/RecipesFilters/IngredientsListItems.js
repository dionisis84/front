import React from "react";
import api from "../../api";
import Typography from "@material-ui/core/Typography";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import Divider from "@material-ui/core/Divider";
import Box from "@material-ui/core/Box";

export default function IngredientsListItems(props){
	// const theAPI = new api();
	const [checked, setChecked] = React.useState([-1]);

	const handleToggle = (value) => () => {
		const currentIndex = checked.indexOf(value);
		const newChecked = [...checked];
	
		if (currentIndex === -1) {
		  newChecked.push(value);
		} else {
		  newChecked.splice(currentIndex, 1);
		}
	
		setChecked(newChecked);
	};
	
	return(
		<React.Fragment>
			<Divider />
			<ListItem>
				<Typography variant="h6">Ingredients</Typography>
			</ListItem>

			<Box style={{
				height: "30vh",
				overflow: "auto"
			}}>
				{api.getIngredients().map((ing, index) => (
					<ListItem button dense key={index} onClick={handleToggle(index)}>
						<ListItemIcon>
							<Checkbox
								edge="start"
								checked={checked.indexOf(index) !== -1}
								disableRipple
								color="primary"
								inputProps={{ 'aria-labelledby': "lblIngFilter" + index }}
							/>
						</ListItemIcon>
						<ListItemText id={"lblIngFilter" + index} primary={ing.name} />
					</ListItem>
				))}
			</Box>
		</React.Fragment>
	);
};