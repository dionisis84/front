import React from "react";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import IngredientsListItems from "./IngredientsListItems";
import CategoriesListItems from "./CategoriesListItems";
import DifficultyListItems from "./DifficultyListItems";
import Typography from "@material-ui/core/Typography";
import LoginPopover from "../LoginPopover";
import LoggedInAvatar from "../LoggedInAvatar";
import faker from "faker";

export default function RecipesFilters(props) {
	const { window } = props;
	const container =
		window !== undefined ? () => window().document.body : undefined;
	const [didLogin, setDidLogin] = React.useState(false);
	const [userInfo, setUserInfo] = React.useState({
		name: faker.name.firstName() + " " + faker.name.lastName(),
		avatar: faker.image.avatar(),
	});

	return (
		<React.Fragment>
			<Hidden mdUp implementation="css">
				<Drawer
					container={container}
					variant="temporary"
					anchor="left"
					open={props.mobileOpen}
					onClose={props.handleDrawerToggle}
					ModalProps={{
						keepMounted: true, // Better open performance on mobile.
					}}
				>
					<List style={{ width: "60vw" }}>
						<ListItem>
							{!didLogin && (
								<LoginPopover
									setDidLogin={setDidLogin}
									color="primary"
									contained={true}
								/>
							)}

							{didLogin && (
								<LoggedInAvatar user={userInfo} setDidLogin={setDidLogin} />
							)}
						</ListItem>

						<ListItem>
							<Typography variant="h5">Filter Results</Typography>
						</ListItem>

						<IngredientsListItems />
						<CategoriesListItems />
						<DifficultyListItems />
					</List>
				</Drawer>
			</Hidden>

			<Hidden smDown implementation="css">
				<Box style={{ paddingTop: "64px" }}>
					<Paper elevation={4}>
						<List>
							<IngredientsListItems />
							<CategoriesListItems />
							<DifficultyListItems />
						</List>
					</Paper>
				</Box>
			</Hidden>
		</React.Fragment>
	);
}
