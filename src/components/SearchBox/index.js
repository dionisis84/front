import React from "react";
import Grid from '@material-ui/core/Grid';
import Autocomplete from "@material-ui/lab/Autocomplete";
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import api from "../../api";
import {ingredients} from "../../data";
import { withRouter } from "react-router";
import StyledTextField from "../StyledTextField";
import Fade from '@material-ui/core/Fade';

class SearchBox extends React.Component{
	state = {
		selectedIngredients: []
	};
	
	render(){
		const { history } = this.props;
		
		return(
			<React.Fragment>
				<Grid item xs sm md lg>
					<Autocomplete
						id="txtIngredientsSearch"
						size="small"
						autoComplete={true}
						multiple
						options={ingredients}
						style={{width: "300px"}}
						limitTags={10}
						getOptionLabel={(ingredient) => ingredient.name}
						renderInput={(params) => <StyledTextField color="secondary" {...params} label="Select your ingredients..." fullWidth={true} autoFocus={true} />}
						onChange={(e, v, r) => {
							this.setState({selectedIngredients: v})
						}}
					/>
				</Grid>
	
				<Divider />
	
					<Fade in={api.searchRecipesCount(this.state.selectedIngredients) > 0}>
						<Button 
							variant="contained" 
							color="secondary" 
							style={{marginTop: "20px"}}
							onClick={() => {
								history.push("/recipes");
							}}
						>
							View {api.searchRecipesCount(this.state.selectedIngredients)} recipes
						</Button>
					</Fade>
			</React.Fragment>
		);
	};
}

export default withRouter(SearchBox);