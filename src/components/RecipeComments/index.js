import React from "react";
import CommentComposer from "../CommentComposer";
import Paper from "@material-ui/core/Paper";
import RecipeCommentsList from "../RecipeCommentsList";
import api from "../../api";
import Divider from "@material-ui/core/Divider";
import Pagination from '@material-ui/lab/Pagination';
import Typography from "@material-ui/core/Typography";

class RecipeComments extends React.Component{
	render(){
		// const theAPI = new api();
		const comments = api.getComments(this.props.recid);
		
		return(
			<Paper style={{padding: "20px"}}>
				<Typography variant="h5">Comments</Typography>

				<CommentComposer />
				<Divider />
				<RecipeCommentsList comments={comments} />
				<Pagination count={10} showFirstButton showLastButton color="primary" style={{marginTop: "20px"}} />
			</Paper>
		);
	}
};

export default RecipeComments;