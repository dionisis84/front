import styled from "styled-components";
import TextField from "@material-ui/core/TextField";

const StyledTextField = styled(TextField)`
	.MuiInput-underline:before {
		border-bottom: 1px solid white;
	}

	.MuiInput-underline:hover:before {
		border-bottom: 2px solid white;
	}

	.MuiInputBase-input{
		color: white;
	}

	.MuiInputLabel-root{
		color: white;
	}

	.MuiAutocomplete-clearIndicator .MuiSvgIcon-root{
		color: white;
	}

	.MuiAutocomplete-popupIndicator .MuiSvgIcon-root{
		color: white;
	}

	.MuiInput-underline.Mui-error:after {
		border-bottom-color: yellow;
	}

	.MuiFormHelperText-root{
		color: yellow;
	}
`;

export default StyledTextField;