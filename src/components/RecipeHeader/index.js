import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Hidden from '@material-ui/core/Hidden';
import Link from '@material-ui/core/Link';
import Button from '@material-ui/core/Button';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import FavoriteIcon from '@material-ui/icons/Favorite';
import PrintIcon from '@material-ui/icons/Print';
import ShareIcon from '@material-ui/icons/Share';
import Paper from "@material-ui/core/Paper";
import LinearProgress from '@material-ui/core/LinearProgress';
import { makeStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import ShareMenu from "../ShareMenu";

function Alert(props) {
	return <MuiAlert elevation={6} variant="filled" {...props} />;
};

const useStyles = makeStyles((theme) => ({
	root: {
		width: '100%',
		'& > * + *': {
		  marginTop: theme.spacing(2),
		},
		position: "absolute",
		bottom: "0px"
	  }
}));

export default function RecipeHeader(props){
	const {img, title, author} = props;
	const [inFavourites, setInFavourites] = React.useState(false);
	const [favLoading, setFavLoading] = React.useState(false);
	const [openFavAlert, setOpenFavAlert] = React.useState(false);
	const [openShare, setOpenShare] = React.useState(false);
	const classes = useStyles();
	const anchorRef = React.useRef(null);

	const handleClick = () => {
		setFavLoading(true);
		setTimeout(() => {
			setInFavourites(!inFavourites);
			setOpenFavAlert(true);
			setFavLoading(false);
		}, 700);
	};

	const handleFavClose = (event, reason) => {
		if (reason === 'clickaway') {
		  return;
		}
	
		setOpenFavAlert(false);
	};

	const handleShareClose = (event) => {
		if (anchorRef.current && anchorRef.current.contains(event.target)) {
			return;
		}

		setOpenShare(false);
	};

	const handleToggleShare = () => {
		setOpenShare((prevOpen) => !prevOpen);
	};
	
	return(
		<React.Fragment>
			<Box 
				style={{
					width: "100%",
					height: "calc(100vh - 104px)",
					overflow: "hidden",
					marginBottom: "30px",
					backgroundImage: `url(${img})`,
					backgroundPosition: "center center",
					backgroundSize: "cover"
				}}
				boxShadow={3}
				display="flex" 
				alignItems="center"
				justifyContent="center"
			>
				<Box style={{
					position:"absolute", 
					textAlign: "center", 
					color: "white", 
					textShadow: "2px 2px 4px rgba(0, 0, 0, 0.5)",
					padding: "10px"
				}}>
					<Hidden only="xs">
						<Typography variant="h1" style={{fontWeight: "bold"}}>{title}</Typography>
					</Hidden>
					<Hidden smUp>
						<Typography variant="h2" style={{fontWeight: "bold"}}>{title}</Typography>
					</Hidden>
					<Link href={author.url} color="inherit" target="_blank" rel="noopener">
						<Typography variant="h5">{author.name}</Typography>
					</Link>

					<Box
						display="flex" 
						alignItems="center"
						justifyContent="center"
						style={{marginTop: "10px"}}
					>
						<Paper style={{
								maxWidth: "200px"
							}}
						>
							<Button color="primary" onClick={handleClick}>
								<div className={classes.root}>{favLoading && <LinearProgress />}</div>
								{!inFavourites && <FavoriteBorderIcon />}
								{inFavourites && <FavoriteIcon />}
							</Button>
							<Button color="primary"><PrintIcon /></Button>
							<Button color="primary" onClick={handleToggleShare} ref={anchorRef}>
								<ShareIcon />
							</Button>
						</Paper>
					</Box>
				</Box>
			</Box>

			<Snackbar open={openFavAlert} autoHideDuration={2000} onClose={handleFavClose}>
				<Alert onClose={handleFavClose} severity="success">
					{inFavourites && "Recipe added succesfully to your favourites!"}
					{!inFavourites && "Recipe removed succesfully from your favourites!"}
				</Alert>
			</Snackbar>

			<ShareMenu open={openShare} anchorRef={anchorRef} handleClose={handleShareClose} />
		</React.Fragment>
	);
};