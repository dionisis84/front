import React from "react";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Paper from '@material-ui/core/Paper';
import Typography from "@material-ui/core/Typography";

export default function IngredientsList(props){
	const {ings} = props;

	return(
		<React.Fragment>
			<Paper style={{padding: "10px", marginBottom: "20px"}}>
				<Typography variant="h5">Ingredients</Typography>
				
				<List>
					{ings.map((ing, idx) => {
						return(<ListItem key={idx}>
							<FormControlLabel
								control={
									<Checkbox
										edge="start" 
										color="primary"
									/>
								}
								label={ing.quantity + " " + ing.quantity_unit + " of " + ing.name}
							/>
						</ListItem>);
					})}
				</List>
			</Paper>
		</React.Fragment>
	);
};