import React from "react";
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";

class CommentComposer extends React.Component{
	render(){
		return(
			<form style={{marginBottom: "20px"}}>
				<TextField 
					required 
					id="txtAuthorName" 
					label="Your name" 
					fullWidth
					margin="dense"
				/>

				<TextField 
					required 
					id="txtAuthorEmail" 
					label="Your email address" 
					type="email" 
					fullWidth
					margin="dense"
				/>

				<TextField
					id="txtAuthorComment"
					label="Your comment"
					multiline
					rows={4}
					fullWidth
					margin="dense"
					required 
				/>

				<Button variant="contained" color="primary" style={{marginTop: "20px"}} type="submit">Post Comment</Button>
			</form>
		);
	}
};

export default CommentComposer;