import React from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Avatar from "@material-ui/core/Avatar";
import MeetingRoomIcon from "@material-ui/icons/MeetingRoom";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import PersonIcon from "@material-ui/icons/Person";
import FavoriteIcon from "@material-ui/icons/Favorite";
import Typography from "@material-ui/core/Typography";
import Hidden from "@material-ui/core/Hidden";
import Divider from "@material-ui/core/Divider";
import Link from "@material-ui/core/Link";

const LoggedInAvatar = (props) => {
	const [anchorEl, setAnchorEl] = React.useState(null);
	const isMenuOpen = Boolean(anchorEl);
	const { user, setDidLogin } = props;

	const handleProfileMenuOpen = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const handleMenuClose = () => {
		setAnchorEl(null);
	};

	const handleLogout = () => {
		setDidLogin(false);
		handleMenuClose();
	};

	return (
		<React.Fragment>
			<Avatar
				src={user.avatar}
				onClick={handleProfileMenuOpen}
				style={{ cursor: "pointer" }}
			/>
			<Hidden mdUp>
				<Typography
					variant="h6"
					style={{ marginLeft: "10px" }}
					onClick={handleProfileMenuOpen}
				>
					{user.name}
				</Typography>
			</Hidden>

			<Menu
				anchorEl={anchorEl}
				anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
				keepMounted
				transformOrigin={{ vertical: "top", horizontal: "right" }}
				open={isMenuOpen}
				onClose={handleMenuClose}
			>
				<Hidden smDown>
					<MenuItem disabled={true}>
						<Typography variant="h6">{user.name}</Typography>
					</MenuItem>
					<Divider />
				</Hidden>

				<Link href="/profile">
					<MenuItem onClick={handleMenuClose}>
						<ListItemIcon>
							<PersonIcon fontSize="small" />
						</ListItemIcon>{" "}
						<ListItemText primary="Profile" />
					</MenuItem>
				</Link>

				<Link href="/profile?fav">
					<MenuItem onClick={handleMenuClose}>
						<ListItemIcon>
							<FavoriteIcon fontSize="small" />
						</ListItemIcon>{" "}
						<ListItemText primary="Favourites" />
					</MenuItem>
				</Link>

				<MenuItem onClick={handleLogout}>
					<ListItemIcon>
						<MeetingRoomIcon fontSize="small" />
					</ListItemIcon>{" "}
					<ListItemText primary="Logout" />
				</MenuItem>
			</Menu>
		</React.Fragment>
	);
};

export default LoggedInAvatar;
