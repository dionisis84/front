import React from "react";
import Paper from '@material-ui/core/Paper';
import Typography from "@material-ui/core/Typography";

export default function RecipeDescription(props){
	// TODO: This component should accept HTML content and display it
	return(
		<Paper style={{padding: "10px", marginBottom: "20px"}}>
			<Typography variant="body1" component="div">
				<div dangerouslySetInnerHTML={{__html: props.content}} />
			</Typography>
		</Paper>
	);
}