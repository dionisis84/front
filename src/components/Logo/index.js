import React from "react";
import Typography from "@material-ui/core/Typography";

export default function Logo(props){
	const {color, variant, project} = props;
	
	return (
	<Typography variant={variant ? variant : "h2"} color={color ? color : "secondary"} style={{fontWeight: "bold"}}>Recipes{project && " Project"}</Typography>
	);
}