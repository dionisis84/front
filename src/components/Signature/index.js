import React from "react";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import Box from "@material-ui/core/Box";

export default function Signature(props) {
	return(
		<Box style={{position: "absolute", bottom: "20px", right: "20px"}}>
			<Link href="https://www.linkedin.com/in/dionisis-kladis-512a413b/" target="_blank" rel="noopener">
				<Typography variant="body2" style={{color: "white"}}>created by Dionisis Kladis</Typography>
			</Link>
		</Box>
	);
};