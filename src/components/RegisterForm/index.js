import React from "react";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Button from "@material-ui/core/Button";
import FacebookIcon from '@material-ui/icons/Facebook';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import EmailIcon from '@material-ui/icons/Email';
import Tooltip from "@material-ui/core/Tooltip";
import TextField from "@material-ui/core/TextField";
import LinearProgress from '@material-ui/core/LinearProgress';
import { withRouter } from "react-router";
class RegisterForm extends React.Component{

	state = {
		name: "",
		email: "",
		username: "",
		pass1: "",
		pass2: "",
		usernameInvalid: false,
		nameInvalid: false,
		emailInvalid: false,
		passInvalid: false,
		registerLoading: false
	};

	checkUsername = () => {
		return Math.round(Math.random()) === 0;
	}

	checkSamePass = (passElem, data) => {
		if(passElem === 1)
		{
			return data === this.state.pass2;
		}
		else if(passElem === 2)
		{
			return this.state.pass1 === data;
		}
	}

	validateField = (field, data) => {
		switch(field){
			case "name":
				this.setState({nameInvalid: data.length < 3 && data.length > 0});
				break;
			case "email":
				this.setState({emailInvalid: data.length < 3 && data.length > 0});	
				break;
			case "username":
				this.setState({usernameInvalid: ((data.length < 3) && (data.length > 0)) || (!this.checkUsername())});		
				break;
			case "password1":
				this.setState({passInvalid: data.length < 3 && data.length > 0 || !this.checkSamePass(1, data)});
				break;
			case "password2":
				this.setState({passInvalid: data.length < 3 && data.length > 0 || !this.checkSamePass(2, data)});
				break;
			default:
				break;
		}
	}

	validateForm = () => {
		this.state.name === "" || this.state.name.length < 3 ? this.setState({nameInvalid: true}) : this.setState({nameInvalid: false});
		this.state.email === "" || this.state.email.length < 3 ? this.setState({emailInvalid: true}) : this.setState({emailInvalid: false});
		this.state.username === "" || this.state.username.length < 3 ? this.setState({usernameInvalid: true}) : this.setState({usernameInvalid: false});
		this.state.pass1 === "" || this.state.pass1.length < 3 ? this.setState({passInvalid: true}) : this.setState({passInvalid: false});
		this.state.pass2 === "" || this.state.pass2.length < 3 ? this.setState({passInvalid: true}) : this.setState({passInvalid: false});
		this.state.pass1 !== this.state.pass2 ? this.setState({passInvalid: true}) : this.setState({passInvalid: false});

		return ((this.state.name !== "" && this.state.name.length > 2) && (this.state.email !== "" && this.state.email.length > 2) && (this.state.username !== "" && this.state.username.length > 2) && (this.state.pass1 !== "" && this.state.pass1.length > 2) && (this.state.pass2 !== "" && this.state.pass2.length > 2) && (this.state.pass1 === this.state.pass2)) ? true : false;
	}

	doRegister = (e) => {
		e.preventDefault();
		const { history } = this.props;
		
		if(this.validateForm())
		{
			this.setState({registerLoading: true});
			setTimeout(() => {
				this.setState({registerLoading: false});
				history.push("/");
			}, 1500);
		}
	};
	
	render(){
		return(
			<form onSubmit={this.doRegister}>
				<TextField label="Enter your name" value={this.state.name} onChange={(e) => {this.setState({name: e.target.value}); this.validateField("name", e.target.value);}} error={this.state.nameInvalid} helperText={this.state.nameInvalid && "Enter at least 3 characters"} fullWidth required margin="dense" />
				<TextField label="Enter your email address" value={this.state.email} onChange={(e) => {this.setState({email: e.target.value}); this.validateField("email", e.target.value);}} error={this.state.emailInvalid} helperText={this.state.emailInvalid && "Enter at least 3 characters"} fullWidth required margin="dense" />
				<TextField label="Enter your desired username" value={this.state.username} onChange={(e) => {this.setState({username: e.target.value}); this.validateField("username", e.target.value);}} helperText={this.state.usernameInvalid && "Username already exists..."} error={this.state.usernameInvalid} fullWidth required margin="dense" />
				<TextField label="Enter a password" value={this.state.pass1} onChange={(e) => {this.setState({pass1: e.target.value}); this.validateField("password1", e.target.value);}} error={this.state.passInvalid} helperText={this.state.passInvalid && "Enter at least 3 characters"} type="password" fullWidth required margin="dense" />
				<TextField label="Enter password again" value={this.state.pass2} onChange={(e) => {this.setState({pass2: e.target.value}); this.validateField("password2", e.target.value);}} error={this.state.passInvalid} helperText={this.state.passInvalid && "Enter at least 3 characters"} type="password" fullWidth required margin="dense" />

				<ButtonGroup variant="contained" style={{marginTop: "40px"}}>
					<Button startIcon={<EmailIcon />} 
						color="primary"
						size="small"
						type="submit"
					>
						Register
						<div style={{width: "100%", position: "absolute", bottom: "0px"}}>{this.state.registerLoading && <LinearProgress />}</div>
					</Button>
					<Tooltip title="Register using your LinkedIn account">
						<Button size="small" startIcon={<LinkedInIcon />} style={{backgroundColor: "#2867B2", color: "white"}}>LinkedIn</Button>
					</Tooltip>
					<Tooltip title="Register using your Facebook account">
						<Button size="small" startIcon={<FacebookIcon />} style={{backgroundColor: "#3b5998", color: "white"}}>Facebook</Button>
					</Tooltip>
				</ButtonGroup>
			</form>
		);
	}
};

export default withRouter(RegisterForm);