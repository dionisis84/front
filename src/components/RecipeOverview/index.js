import React from "react";
import Grid from "@material-ui/core/Grid"
import ScheduleIcon from '@material-ui/icons/Schedule';
import SignalCellularAltIcon from '@material-ui/icons/SignalCellularAlt';
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";

export default function RecipeOverview(props){
	const {prep, cook, wait, diff} = props;
	
	return(
		<Paper style={{marginBottom: "20px", padding: "10px"}}>
			<Grid container justify="center">
				<Grid item xl={2} lg={2} md={2} sm={6} xs={6}>
					<Grid container justify="center">
						<Grid item xs={3} md={4} sm={2}>
							<ScheduleIcon fontSize="large" />
						</Grid>
						<Grid item xs={6} sm={2}>
							<div><Typography variant="body2" style={{fontWeight: "300"}}>Preparation</Typography></div>
							<div><Typography variant="body1" color="primary" style={{fontWeight: "bold"}}>{prep}</Typography></div>
						</Grid>
					</Grid>
				</Grid>
				<Grid item xl={2} lg={2} md={2} sm={6} xs={6}>
					<Grid container justify="center">
						<Grid item xs={3} md={4} sm={2}>
							<ScheduleIcon fontSize="large" />
						</Grid>
						<Grid item xs={6} sm={2}>
							<div><Typography variant="body2" style={{fontWeight: "300"}}>Waiting</Typography></div>
							<div><Typography variant="body1" color="primary" style={{fontWeight: "bold"}}>{wait}</Typography></div>
						</Grid>
					</Grid>
				</Grid>
				<Grid item xl={2} lg={2} md={2} sm={6} xs={6}>
					<Grid container justify="center">
						<Grid item xs={3} md={4} sm={2}>
							<ScheduleIcon fontSize="large" />
						</Grid>
						<Grid item xs={6} sm={2}>
							<div><Typography variant="body2" style={{fontWeight: "300"}}>Cooking</Typography></div>
							<div><Typography variant="body1" color="primary" style={{fontWeight: "bold"}}>{cook}</Typography></div>
						</Grid>
					</Grid>
				</Grid>
				<Grid item xl={2} lg={2} md={2} sm={6} xs={6}>
					<Grid container justify="center">
						<Grid item xs={3} md={4} sm={2}>
							<ScheduleIcon fontSize="large" />
						</Grid>
						<Grid item xs={6} sm={2}>
							<div><Typography variant="body2" style={{fontWeight: "300"}}>Duration</Typography></div>
							<div><Typography variant="body1" color="primary" style={{fontWeight: "bold"}}>{prep + cook + wait}</Typography></div>
						</Grid>
					</Grid>
				</Grid>
				<Grid item xl={2} lg={2} md={2} sm={12} xs={6}>
					<Grid container justify="center">
						<Grid item xs={3} md={4} sm={1}>
							<SignalCellularAltIcon fontSize="large" />
						</Grid>
						<Grid item xs={6} sm={2}>
							<div><Typography variant="body2" style={{fontWeight: "300"}}>Difficulty</Typography></div>
							<div><Typography variant="body1" color="primary" style={{fontWeight: "bold"}}>{diff}</Typography></div>
						</Grid>
					</Grid>
				</Grid>
			</Grid>
		</Paper>
	);
};