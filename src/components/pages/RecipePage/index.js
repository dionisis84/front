import React from "react";
import TopBar from "../../TopBar";
import RecipeHeader from "../../RecipeHeader";
import Container from "@material-ui/core/Container";
import IngredientsList from "../../IngredientsList";
import RecipeDescription from "../../RecipeDescription";
import RecipeSteps from "../../RecipeSteps";
import RecipeComments from "../../RecipeComments";
import RecipeOverview from "../../RecipeOverview";
import api from "../../../api";
import Footer from "../../Footer";

class RecipePage extends React.Component {
	state = {
		recipeDetails: undefined,
	};

	async componentDidMount() {
		this.setState({ recipeDetails: await api.getRecipeDetails(2) });
	}

	render() {
		return (
			<React.Fragment>
				<TopBar />
				{this.state.recipeDetails && (
					<React.Fragment>
						<RecipeHeader
							img={this.state.recipeDetails.imageUrl}
							title={this.state.recipeDetails.title}
							author={this.state.recipeDetails.author}
						/>

						<Container fixed>
							<RecipeOverview
								prep={this.state.recipeDetails.overview.preparation}
								cook={this.state.recipeDetails.overview.cook}
								wait={this.state.recipeDetails.overview.wait}
								diff={this.state.recipeDetails.overview.difficulty}
							/>
							<RecipeDescription
								content={this.state.recipeDetails.description}
							/>
							<IngredientsList ings={this.state.recipeDetails.ingredients} />
							<RecipeSteps steps={this.state.recipeDetails.steps} />
							<RecipeComments recid={this.state.recipeDetails.id} />
						</Container>
					</React.Fragment>
				)}

				<Footer />
			</React.Fragment>
		);
	}
}

export default RecipePage;
