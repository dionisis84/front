import React from "react";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Signature from "../../Signature";
import Logo from "../../Logo";
import {theme} from "../../Theme";
import RegisterForm from "../../RegisterForm";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";

export default function RegisterUserPage(props) {
	return(
		<Box style={{backgroundColor: theme.palette.primary.main}}>
			<Grid 
				container 
				alignItems="center" 
				direction="column" 
				justify="center" 
				style={{
					 minHeight: '100vh'
				}}
			>
				<Grid item xs={12} sm={12} md={12} lg={12} style={{textAlign: "center"}}>
					<Logo />
				</Grid>
			
				<Grid item xs={12} sm={12} md={12} lg={12}>
					<Container maxWidth="xs">
						<Card style={{padding: "20px", textAlign: "center"}} raised={true}>
							<Typography variant="h4">Register Account</Typography>
							
							<RegisterForm />
						</Card>
					</Container>
				</Grid>
			</Grid>

			<Signature />
		</Box>
	);
};