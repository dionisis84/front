import React from "react";
import TopBar from "../../TopBar";
import RecipesList from "../../RecipesList";
import Box from "@material-ui/core/Box";
import RecipesFilters from "../../RecipesFilters";
import LinearProgress from "@material-ui/core/LinearProgress";
import Footer from "../../Footer";
import Grid from "@material-ui/core/Grid";
class RecipesPage extends React.Component {
	state = {
		loading: true,
		mobileOpen: false,
	};

	handleDrawerToggle = () => {
		this.setState({ mobileOpen: !this.state.mobileOpen });
	};

	componentDidMount = () => {
		setTimeout(() => {
			this.setState({ loading: false });
		}, 1500);
	};

	render() {
		return (
			<React.Fragment>
				<TopBar
					title="Search Results"
					showDrawer={true}
					handleDrawerToggle={this.handleDrawerToggle}
				/>
				{this.state.loading && <LinearProgress />}

				<Grid container>
					<Grid item xl={2} lg={2} md={2}>
						<RecipesFilters
							mobileOpen={this.state.mobileOpen}
							handleDrawerToggle={this.handleDrawerToggle}
						/>
					</Grid>

					<Grid item xl={10} lg={10} md={10} sm={12} xs={12}>
						<Box style={{ paddingTop: "64px" }}>
							<RecipesList loading={this.state.loading} />
						</Box>
					</Grid>
				</Grid>

				<Footer />
			</React.Fragment>
		);
	}
}

export default RecipesPage;
