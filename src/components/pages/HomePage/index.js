import React from "react";
import Logo from "../../Logo";
import SearchBox from "../../SearchBox";
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Box from "@material-ui/core/Box"
import {theme} from "../../Theme";
import Signature from "../../Signature";

export default function HomePage(props){
	return(
		<Box style={{backgroundColor: theme.palette.primary.main}}>
			<Grid 
				container 
				alignItems="center" 
				direction="column" 
				justify="center" 
				style={{
					 minHeight: '100vh' 
				}}
			>
				<Grid item xs={12} sm={12} md={12} lg={12}>
					<Logo />
				</Grid>
			
				<Grid item xs={12} sm={12} md={12} lg={12}>
					<Container>
						<SearchBox />
					</Container>
				</Grid>
			</Grid>

			<Signature />
		</Box>
	);
}