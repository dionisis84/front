import React from "react";
import PropTypes from "prop-types";
import TopBar from "../../TopBar";
import Footer from "../../Footer";
import api from "../../../api";
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import RecipesList from "../../RecipesList";
import { withRouter } from "react-router";

function TabPanel(props) {
	const { children, value, index, ...other } = props;

	return (
		<div
			role="tabpanel"
			hidden={value !== index}
			id={`simple-tabpanel-${index}`}
			aria-labelledby={`simple-tab-${index}`}
			{...other}
		>
			{value === index && (
				<Box p={3}>
					<Typography>{children}</Typography>
				</Box>
			)}
		</div>
	);
}

TabPanel.propTypes = {
	children: PropTypes.node,
	index: PropTypes.any.isRequired,
	value: PropTypes.any.isRequired,
};

class ProfilePage extends React.Component {
	state = {
		userData: undefined,
		tabValue: this.props.location.search.includes("fav") ? 1 : 0,
		favsLoading: true,
	};

	async componentDidMount() {
		this.setState({ userData: api.getUserData() });

		setTimeout(() => {
			this.setState({ favsLoading: false });
		}, 1500);
	}

	handleChangeTab = (event, newValue) => {
		this.setState({ tabValue: newValue });
	};

	handleChangeIndex = (index) => {
		this.setState({ tabValue: index });
	};

	render() {
		return (
			<React.Fragment>
				<TopBar />
				{this.state.userData && (
					<React.Fragment>
						<Box
							style={{
								width: "100%",
								height: "calc(100vh - 104px)",
								overflow: "hidden",
								backgroundImage: `url(${this.state.userData.cover})`,
								backgroundPosition: "center center",
								backgroundSize: "cover",
							}}
						></Box>

						<Box
							display="flex"
							alignItems="center"
							justifyContent="center"
							style={{
								position: "relative",
								top: "-80px",
							}}
						>
							<Container maxWidth="md" style={{ position: "relative" }}>
								<Avatar
									alt="Remy Sharp"
									src={this.state.userData.avatar}
									style={{
										position: "absolute",
										top: "-60px",
										left: "calc(50% - 60px)",
										height: "120px",
										width: "120px",
										boxShadow: "rgba(0, 0, 0, 0.3) 3px 3px 5px",
									}}
									boxShadow={3}
								/>
								<Paper elevation={3} style={{ paddingTop: "60px" }}>
									<Box style={{ textAlign: "center", marginTop: "10px" }}>
										<Typography variant="h4">
											{this.state.userData.name}
										</Typography>
									</Box>

									<Tabs
										value={this.state.tabValue}
										onChange={this.handleChangeTab}
										indicatorColor="primary"
										textColor="primary"
										centered
									>
										<Tab label="General Information" />
										<Tab label="Favourites" />
									</Tabs>

									<TabPanel
										value={this.state.tabValue}
										index={0}
										style={{ textAlign: "center" }}
									>
										<Typography
											variant="body2"
											style={{ marginBottom: "20px" }}
										>
											{"Joined: " +
												this.state.userData.joined.toString().split("(")[0]}
										</Typography>

										<Typography variant="body1">
											{this.state.userData.bio}
										</Typography>
									</TabPanel>
									<TabPanel value={this.state.tabValue} index={1}>
										<RecipesList
											loading={this.state.favsLoading}
											recipes={this.state.userData.favourites}
										/>
									</TabPanel>
								</Paper>
							</Container>
						</Box>
					</React.Fragment>
				)}
				<Footer />
			</React.Fragment>
		);
	}
}

export default withRouter(ProfilePage);
