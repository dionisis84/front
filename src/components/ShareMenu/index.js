import React from "react";
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import PinterestIcon from '@material-ui/icons/Pinterest';
import TwitterIcon from '@material-ui/icons/Twitter';
import Divider from "@material-ui/core/Divider";

export default function ShareMenu(props){
	const {open, anchorRef, handleClose} = props;
	
	return(
		<React.Fragment>
			<Popper open={open} anchorEl={anchorRef.current} transition>
			{({ TransitionProps, placement }) => (
				<Grow
				{...TransitionProps}
				style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
				>
				<Paper>
					<ClickAwayListener onClickAway={handleClose}>
					<MenuList autoFocusItem={open} id="menu-list-grow">
						<MenuItem onClick={handleClose}>
							<ListItemIcon style={{color: "#3b5998"}}><FacebookIcon /></ListItemIcon>
							<ListItemText style={{color: "#3b5998"}}>Facebook</ListItemText>
						</MenuItem>
						<Divider />
						<MenuItem onClick={handleClose}>
							<ListItemIcon style={{color: "#3f729b"}}><InstagramIcon /></ListItemIcon>
							<ListItemText style={{color: "#3f729b"}}>Instagram</ListItemText>
						</MenuItem>
						<Divider />
						<MenuItem onClick={handleClose}>
							<ListItemIcon style={{color: "#c8232c"}}><PinterestIcon /></ListItemIcon>
							<ListItemText style={{color: "#c8232c"}}>Pinterest</ListItemText>
						</MenuItem>
						<Divider />
						<MenuItem onClick={handleClose}>
							<ListItemIcon style={{color: "#00acee"}}><TwitterIcon /></ListItemIcon>
							<ListItemText style={{color: "#00acee"}}>Twitter</ListItemText>
						</MenuItem>
					</MenuList>
					</ClickAwayListener>
				</Paper>
				</Grow>
			)}
			</Popper>
		</React.Fragment>
	);
}